# Отчёт по практической работе номер 3

## Задание

Используя функции, реализовать программу, которая:
1. cчитывает количество элементов n, 0 < n <= 2147483647;
2. создает пустой список, считывает n элементов a, |a| <= 2147483647 и заносит их в список;
3. выводит содержимое списка, используя функцию print;
4. считывает k1, k2, k3 (|k| <= 2147483647) и выводит "1", если в списке существует элемент с таким значением и "0", если нет (выводить через пробел, например "1 0 1");
5. считывает m, |m| <= 2147483647 и вставляет его в конец списка;
6. выводит содержимое списка, используя функцию print;
считывает t, |t| <= 2147483647 и вставляет его в начало списка;
7. выводит содержимое списка, используя функцию print;
8. считывает j и x (0 < j <= 2147483647, |x| <= 2147483647) и вставляет значение x после j-ого элемента списка;
выводит содержимое списка, используя функцию print;
9. считывает z, |z| <= 2147483647 и удаляет первый элемент (при его наличии), хранящий значение z из списка;
10. выводит содержимое списка, используя функцию print;
очищает список.

## Написание кода

Был написан код, приведённый ниже

```
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


typedef struct node {
  int value;          // значение, которое хранит узел 
  struct node *next;  // ссылка на следующий элемент списка
} node;

typedef struct list {
  struct node *head;  // начало
  struct node *end;  // конец
} list;

// инициализация пустого списка
void init(list *l);

// удалить все элементы из списка
void clean(list *l);

// проверка на пустоту списка
bool is_empty(list *l);

// поиск элемента по значению. вернуть NULL если эжемент не найден
node *find(list *l, int value);

// вставка значения в конец списка, вернуть 0 если успешно
int push_back(list *l, int value);

// вставка значения в начало списка, вернуть 0 если успешно
int push_front(list *l, int value);

// вставка значения после указанного узла, вернуть 0 если успешно
int insert_after(list *i, int index, int value);

// удалить первый элемент из списка с указанным значением, 
// вернуть 0 если успешно
int remove_node(list *l, int value);

// вывести все значения из списка в прямом порядке через пробел,
// после окончания вывода перейти на новую строку
void print(list *l);

int main() {
  // Место для вашего кода
  list mutalisk;
  init(&mutalisk);
  int n, i, a;
  scanf("%d", &n);
  for (i = 0; i<n; ++i) {
    scanf("%d", &a);
    if (push_back(&mutalisk, a) !=0 ) {
      printf("failed to push back\n");
    }
  }
  print(&mutalisk);
  for (i = 0; i<3; ++i) {
    scanf("%d", &a);
    if (find(&mutalisk, a) != NULL) {
      printf("1 ");
    }
    else {
      printf ("0 ");
    }
  }
  printf("\n");
  scanf("%d", &a);
  if (push_back(&mutalisk, a) != 0) {
    printf("failed to push back\n");
  }
  print(&mutalisk);
  scanf("%d", &a);
  if (push_front(&mutalisk, a) != 0) {
    printf("failed to push front\n");
  }
  print(&mutalisk);
  scanf("%d%d", &i, &a);
  if (insert_after(&mutalisk, i, a) != 0) {
    printf("failed to insert after\n");
  }
  print(&mutalisk);
  scanf("%d", &a);
  remove_node(&mutalisk, a);
  print(&mutalisk);
  clean(&mutalisk);
  return 0;
};

void init(list *l)
{
  l->head = NULL;
  l->end = NULL;
}

void clean(list *l) {
  if (is_empty(l)) {
    perror("clean on empty list");
    return;
  }
  node *foo = NULL;
  node *bar = NULL;
  foo = l->head;
  while (foo->next != NULL) {
    bar = foo->next;
    free(foo);
    foo=bar;
  }
  free(foo);
  l->head = NULL;
  l->end = NULL;
}

bool is_empty (list *l) {
  if (l->head == NULL)
    return 1;
  else
    return 0;
}

node *find(list *l, int value) {
  node *foo = NULL;
  foo = l->head;
  while (foo != l->end) {
    if (foo->value == value)
      return foo;
    foo = foo->next;
  }
  if (foo->value == value)
    return foo;
  return NULL;
}

int push_back(list *l, int value)
{
  node *foo = malloc(sizeof(node));
  if (foo == NULL) {
    perror("allocation error");
    return 1;
  }
  foo->value = value;
  foo->next = NULL;
  if (is_empty(l)) {
    l->head = foo;
    l->end = l->head;
  }
  else {
    l->end->next = foo;
    l->end = foo;
  }
  return 0;
}

int push_front(list *l, int value) {
  node *foo = malloc(sizeof(node));
  if (foo == NULL) {
    perror("allocation error");
    return 1;
  }
  foo->value = value;
  foo->next = l->head;
  l->head = foo;
  return 0;
}

int insert_after(list *l, int index, int value) {
  if (index < 0) {
    perror("index < 1");
    return 1;
  }
  node *foo = malloc(sizeof(node));
  if (foo == NULL) {
    perror("allocation error");
    return 1;
  }
  foo->value = value;
  foo->next = NULL;
  if (index == 0) {
    push_front(l, value);
  }
  node *bar = NULL;
  bar = l->head;
  int i;
  for (i = 1; i<index; ++i) {
    if (bar->next == NULL) {     //введен индекс больше количества элементов
         return 1;
    }
    bar = bar->next;
  }
  if (bar == l->end) {   //индекс оказался концом списка
    l->end = foo;
  }
  foo->next = bar->next;
  bar->next = foo;
  return 0;
}

int remove_node(list *l, int value) {
  if (is_empty(l)) {
    return 1;
  }
  node *foo = NULL;
  foo = l->head;
  if (l->head->value == value) {
    if (l->head->next == NULL) {
      free(l->head);
      l->head = NULL;
      return 0;
    }
    foo = l->head->next;
    free(l->head);
    l->head = foo;
    return 0;
  }
  node* bar = NULL;
  while (foo->next != NULL) {
    if (foo->next->value == value) {
      if (foo->next == l->end) {
  l->end = foo;
      }
      bar = foo->next->next;
      free(foo->next);
      foo->next = bar;
      return 0;
    }
    foo = foo->next;
  }
  return 1;
}

void print(list *l) {
  node *foo = NULL;
  foo = l->head;
  while (foo->next != NULL) {
    printf("%d ", foo->value);
    foo = foo->next;
  }
  printf("%d\n", foo->value);
}
```


## Настройка pipeline

Необходимо скопировать `pr3/tests/checker.py` c [основного репозитория](https://gitlab.com/tusur_fb_timp/practices "ссылка") к себе. `checker.py` должен находиться в pr3/tests.

Найти пункт stages в .gitlab_ci.yml и добавить pr3 (Рисунок 1)

![Рисунок 1](pics/stages.jpg "Рисунок 1 - stages")

Рисунок 1 - Пункт stages

Далее нужно раскомментировать строки в `.gitlab-ci.yml` (Рисунок 2)

![Рисунок 2](pics/yml.jpg "Рисунок 2 - yml")

Рисунок 2 - Настройка yml

## Заключение

|input    |output         |
|:-------:|:-------------:|
|5        |               |
|1 2 3 2 4|1 2 3 2 4      |
|2 5 1    |1 0 1          |
|5        |1 2 3 2 4 5    |
|7        |7 1 2 3 2 4 5  |
|3 0      |7 1 2 0 3 2 4 5|
|2        |7 1 0 3 2 4 5  |

Таблица 1 - Входные и выходные данные программы

![Рисунок 3](pics/code.jpg "Рисунок 3 - code")

Рисунок 3 - Результаты работы программы

![Рисунок 4](pics/pipeline.jpg "Рисунок 4 - pipeline")

Рисунок 4 - Пройденный pipeline