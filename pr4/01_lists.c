#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

typedef struct node
{
 int* value;
 struct node* next;
 struct node* prev;
}
node;

typedef struct list
{
 struct node* head;
 struct node* tail;
}
list;

// инициализация пустого списка
void init(list* l)
{
 l->head = l->tail = NULL;
}

// удалить все элементы из списка
void clean(list* l)
{
 node* bar = NULL;
 while ((l->head->next) != NULL)
 {
  bar = l->head;
  l->head = l->head->next;
  free(bar);
 }
 free(l->head);
}

// проверка на пустоту списка
bool is_empty(list* l)
{
 node* bar = l->head;
 if (bar != NULL)
  return 1;
}

// поиск элемента по значению. вернуть NULL если элемент не найден
node* find(list* l, int value)
{
 node* bar = l->head;
 while ((bar != NULL) && (bar->value != value))
  bar = bar->next;
 return bar;
}

// вставка значения в конец списка, вернуть 0 если успешно
int push_back(list* l, int value)
{
 node* bar = (node*)malloc(sizeof(node));
 if (bar == NULL)
  exit(3);
 bar->value = value;
 bar->next = NULL;
 bar->prev = l->tail;
 if (l->tail)
  l->tail->next = bar;
 l->tail = bar;
 if (l->head == NULL)
  l->head = bar;
 return 0;
}

// вставка значения в начало списка, вернуть 0 если успешно
int push_front(list* l, int value)
{
 node* bar = (node*)malloc(sizeof(node));
 if (bar == NULL)
  exit(3);
 bar->value = value;
 bar->next = l->head;
 bar->prev = NULL;
 if (l->head)
  l->head->prev = bar;
 l->head = bar;
 if (l->tail == NULL)
  l->tail = bar;
 return 0;
}

// вставка значения после указанного узла, вернуть 0 если успешно
int insert_after(list* l, node* n, int value)
{
 if (n && n->next)
 {
  node* bar = malloc(sizeof(node));
  bar->value = value;
  bar->next = n->next;
  bar->prev = n;
  n->next = bar;
  n->next->next->prev = bar;
  return 0;
 }
 else
 {
  push_back(l, value);
  return 0;
 }
 return -1;
}

// вставка значения перед указанным узлом, вернуть 0 если успешно
int insert_before(list* l, node* n, int value)
{
 if (n && n->prev)
 {
  node* bar = malloc(sizeof(node));
  bar->value = value;
  bar->prev = n->prev;
  bar->next = n;
  n->prev = bar;
  n->prev->prev->next = bar;
  return 0;
 }
 else
 {
  push_front(l, value);
  return 0;
 }
 return -1;
}

// удалить первый элемент из списка с указанным значением,
// вернуть 0 если успешно
int remove_first(list* l, int value)
{
 if (l->head == NULL)
  return -1;
 node* bar = l->head;
 while (bar->next)
 {
  if (bar->value == value)
  {
   if (bar->prev == NULL)
   {
    l->head = bar->next;
    bar->next->prev = NULL;
    free(bar);
    return 0;
   }
   if (bar->next == NULL)
   {
    l->tail = bar->prev;
    bar->prev->next = NULL;
    free(bar);
    return 0;
   }
   bar->prev->next = bar->next;
   bar->next->prev = bar->prev;
   free(bar);
   return 0;
  }
  bar = bar->next;
 }
 return -1;
}

// удалить последний элемент из списка с указанным значением, 
// вернуть 0 если успешно
int remove_last(list* l, int value)
{
 if (l->tail == NULL)
  return -1;
 node* bar = l->tail;
 while (bar->prev)
 {
  if (bar->value == value)
  {
   if (bar->next == NULL)
   {
    l->tail = bar->prev;
    bar->prev->next = NULL;
    free(bar);
    return 0;
   }
   if (bar->prev == NULL)
   {
    l->head = bar->next;
    bar->next->prev = NULL;
    free(bar);
    return 0;
   }
   bar->prev->next = bar->next;
   bar->next->prev = bar->prev;
   free(bar);
   return 0;
  }
  bar = bar->prev;
 }
 return -1;
}

// вывести все значения из списка в прямом порядке через пробел,
// после окончания вывода перейти на новую строку
void print(list* l)
{
 node* bar = l->head;
 while (bar != NULL)
 {
  printf("%d ", bar->value);
  bar = bar->next;
 }
}

// вывести все значения из списка в обратном порядке через пробел,
// после окончания вывода перейти на новую строку
void print_invers(list* l)
{
 node* bar = l->tail;
 do
 {
  printf("%d ", bar->value);
  bar = bar->prev;
 } while (bar != NULL);
}

// проверка наличия элементов
void is_element(list* l, int value)
{
 if (find(l, value) != NULL)
  printf("1 ");
 else
 printf("0 ");
}

node* search(list* l, int idx)
{
 node* bar = l->head;
 int num = 0;
 while ((bar != NULL) && (num != idx - 1))
 {
  bar = bar->next;
  num++;
 }
 return bar;
}

int main()
{
 int n, num, cell;
 int i = 1;
 list l = { 0 };
 int check = scanf("%d", &n);
 assert(check == 1);
 while (i <= n)
 {
  check = scanf("%d", &num);
  assert(check == 1);
  push_back(&l, num);
  i++;
 }
 print(&l);
 printf("\n");
//нахождение элементов в списке
 i = 1;
 while (i <= 3)
 {
  check = scanf("%d", &num);
  assert(check == 1);
  is_element(&l, num);
  i++;
 }
 printf("\n");
//добавление в конец
 check = scanf("%d", &num);
 assert(check == 1);
 push_back(&l, num);
 print_invers(&l);
 printf("\n");
//добавление в начало
 check = scanf("%d", &num);
 assert(check == 1);
 push_front(&l, num);
 print(&l);
 printf("\n");
//добавление после указанного элемента
 check = scanf("%d", &num);
 assert(check == 1);
 check = scanf("%d", &cell);
 assert(check == 1);
 node* insert_head = search(&l, num);
 insert_after(&l, insert_head, cell);
 print_invers(&l);
 printf("\n");
//добавление перед указанным элементом
 check = scanf("%d", &num);
 assert(check == 1);
 check = scanf("%d", &cell);
 assert(check == 1);
 node* insert_tail = search(&l, num);
 insert_before(&l, insert_tail, cell);
 print(&l);
 printf("\n");
//удаление первого элемента равного введенному
 check = scanf("%d", &num);
 assert(check == 1);
 remove_first(&l, num);
 print_invers(&l);
 printf("\n");
//удаление последнего элемента равного введенному
 check = scanf("%d", &num);
 assert(check == 1);
 remove_last(&l, num);
 print(&l);
 printf("\n");
//очищение списка
 clean(&l);
 return 0;
}
